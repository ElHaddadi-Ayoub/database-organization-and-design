﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Food_Order.Models
{
    public class User
    {

        public string user_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string user_modifier_date_time { get; set; }
    

    }
    public class food_items
    {
        public string food_id { get; set; }
        public string food_name { get; set; }
        public string food_price { get; set; }
        public string discount { get; set; }
    }

    public class Order
    {
        public string order_number { get; set; }
        public string date_of_purchase { get; set; }
        public string user_id { get; set; }
        public string food_id { get; set; }
    }

    public class Data
    {
        public List<User> users { get; set; }
        public List<food_items> food_items { get; set; }
        public List<Order> orders { get; set; }
    }
}