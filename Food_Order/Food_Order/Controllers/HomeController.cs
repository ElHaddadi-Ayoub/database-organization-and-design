﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Food_Order.Models;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Food_Order.Controllers
{
    public class HomeController : Controller
    {
        SqlCommand com = new SqlCommand();
        SqlDataReader dr;
        SqlConnection con = new SqlConnection();
        Data data = new Data();
        public List<User> users = new List<User>();
        public List<food_items> food_items = new List<food_items>();
        public List<Order> orders = new List<Order>();

        public HomeController()
        {
            con.ConnectionString = Food_Order.Properties.Resources.ConnectionString;
        }
        public ActionResult Index()
        {
            FetchTable1();
            FetchTable2();
            FetchTable3();
            return View(data);
        }

        private void FetchTable1()
        {
            if(users.Count() > 0)
            {
                users.Clear();
            }
            try
            {
                con.Open();
                com.Connection = con;
                com.CommandText = "SELECT TOP (1000) [user_id] ,[first_name] ,[last_name] ,[email] ,[phone_number], [user_modifier_date_time] FROM [Food_Order].[dbo].[Users]";
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    users.Add(new User() {user_id = dr["user_id"].ToString(),first_name = dr["first_name"].ToString(), last_name = dr["last_name"].ToString(),
                                           email = dr["email"].ToString(),
                        phone_number = dr["phone_number"].ToString(), user_modifier_date_time = dr["user_modifier_date_time"].ToString()});
                }
                data.users = users;
                con.Close();
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        
        private void FetchTable2()
        {
            if (food_items.Count() > 0)
            {
                food_items.Clear();
            }
            try
            {
                con.Open();
                com.Connection = con;
                com.CommandText = "SELECT TOP (1000) [food_id] ,[food_name] ,[food_price] ,[discount] FROM[Food_Order].[dbo].[Food_items]";
                dr = com.ExecuteReader();
              while (dr.Read())
                {
                    food_items.Add(new food_items()
                    {
                        food_id = dr["food_id"].ToString(),
                        food_name = dr["food_name"].ToString(),
                        food_price = dr["food_price"].ToString(),
                        discount = dr["discount"].ToString()
                    });
                }
                data.food_items = food_items;
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FetchTable3()
        {
            if (orders.Count() > 0)
            {
                orders.Clear();
            }
            try
            {
                con.Open();
                com.Connection = con;
                com.CommandText = "SELECT TOP (1000) [order_number] ,[date_of_purchase] ,[user_id] ,[food_id] FROM[Food_Order].[dbo].[Orders]";
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    orders.Add(new Order()
                    {
                        order_number = dr["order_number"].ToString(),
                        date_of_purchase = dr["date_of_purchase"].ToString(),
                        user_id = dr["user_id"].ToString(),
                        food_id = dr["food_id"].ToString()
                    });
                }
                data.orders = orders;
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void add_user_todb(string v1, string v2, string v3, string v4)
        {
            con.Open();
            com.Connection = con;
            com.CommandText = "USE [Food_Order] GO INSERT INTO[dbo].[Users] ([first_name] ,[last_name] ,[email] ,[phone_number]) VALUES(" +v1+ ", " + v2+ ", " + v3 + ", " + v4 + ") GO";
            com.ExecuteNonQuery();
            con.Close();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Food Order";

            return View();
        }

       /* public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }*/
    }
}